package com.example.demo;

import com.example.demo.entity.Student;
import com.example.demo.entity.StudentGroup;
import com.example.demo.entity.Teacher;
import com.example.demo.entity.University;
import com.example.demo.repository.StudentGroupRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.TeacherRepository;
import com.example.demo.repository.UniversityRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@RestController
@RequestMapping("/database")
public class Controller {

    @Autowired
    UniversityRepository universityRepository;
    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    StudentGroupRepository studentGroupRepository;
    @Autowired
    StudentRepository studentRepository;

    @RequestMapping("/save")
    public String saveData() {
        for(int i = 0; i < 10; i++) {
            way1();
        }
        return "ok";
    }

    public void way1() {
        Faker faker = new Faker();
        University university = universityRepository.save(University.builder().name(faker.company().name()).build());
        LinkedList<Student> students = new LinkedList();
        LinkedList<Teacher> teachers = new LinkedList();
        LinkedList<StudentGroup> groups = new LinkedList();
        for(int i = 0; i < 40; i++) {
            teachers.add(
                    Teacher.builder()
                            .firstName(faker.name().firstName())
                            .lastName(faker.name().lastName())
                            .salary(ThreadLocalRandom.current().nextInt(2000, 8000))
                            .university(university)
                            .build()
            );
        }
        List<Teacher> teachersSaved = teacherRepository.saveAll(teachers);
        for(Teacher teacher : teachersSaved) {
            for(int i = 0; i < 8; i++) {
                groups.add(StudentGroup.builder()
                        .name(faker.name().title())
                        .teacher(teacher)
                        .build());
            }
        }
        List<StudentGroup> groupsSaved = studentGroupRepository.saveAll(groups);
        for(StudentGroup studentGroup : groupsSaved) {
            for(int i = 0; i < 30; i++){
                students.add(
                        Student.builder()
                                .firstName(faker.name().firstName())
                                .lastName(faker.name().lastName())
                                .mark(ThreadLocalRandom.current().nextInt(2, 5))
                                .university(university)
                                .studentGroup(studentGroup)
                                .build()
                );
            }
        }
        studentRepository.saveAll(students);
    }
    public void way2() {
        Faker faker = new Faker();
        LinkedList<University> universities = new LinkedList();
        LinkedList<Student> students = new LinkedList();
        LinkedList<Teacher> teachers = new LinkedList();
        LinkedList<StudentGroup> groups = new LinkedList();



        University university = University.builder().name(faker.company().name()).build();
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 1000; j++) {
                students.add(
                        Student.builder()
                                .firstName(faker.name().firstName())
                                .lastName(faker.name().lastName())
                                .mark(ThreadLocalRandom.current().nextInt(2, 5))
                                .university(university)
                                .build()
                );
            }

            Teacher teacher = Teacher.builder()
                    .firstName(faker.name().firstName())
                    .lastName(faker.name().lastName())
                    .salary(ThreadLocalRandom.current().nextInt(2000, 8000))
                    .studentGroups(groups)
                    .university(university)
                    .build();

            for(int j = 0; j < 10; j++) {
                StudentGroup studentGroup = StudentGroup.builder()
                        .name(faker.name().title())
                        .teacher(teacher)
                        .build();
                for(int k = j*10; k < (j+1)*10; k++) {
                    students.get(k).setStudentGroup(studentGroup);
                }
                groups.add(studentGroup);
            }

            teachers.add(teacher);
        }
        university.setStudents(students);
        university.setTeachers(teachers);
        universities.add(university);
        universityRepository.saveAll(universities);
    }
}

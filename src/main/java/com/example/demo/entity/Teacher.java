package com.example.demo.entity;

import lombok.Builder;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Builder
@Entity
public class Teacher {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column
    public String firstName;

    @Column
    public String lastName;

    @Column
    public int salary;

    @OneToMany(mappedBy = "teacher", cascade = ALL)
    List<StudentGroup> studentGroups;

    @ManyToOne
    public University university;

}

package com.example.demo.entity;

import lombok.Builder;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Builder
@Entity
public class StudentGroup {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column
    public String name;

    @OneToMany(mappedBy = "studentGroup", cascade = ALL)
    List<Student> students;

    @ManyToOne
    public Teacher teacher;
}

package com.example.demo.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Builder
@Getter
@Setter
@Entity
public class University {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column
    public String name;

    @OneToMany(mappedBy = "university", cascade = ALL)
    List<Teacher> teachers;

    @OneToMany(mappedBy = "university", cascade = ALL)
    List<Student> students;
}

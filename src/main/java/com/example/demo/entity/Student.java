package com.example.demo.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Builder
@Entity
@Getter
@Setter
@Table(name = "Student")
public class Student {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column
    public String firstName;

    @Column
    public String lastName;

    @Column
    public int mark;

    @ManyToOne
    public StudentGroup studentGroup;

    @ManyToOne
    public University university;
}
